Cristina Ungurean
Grupa 233

Codul este pe doua brach-uri:
1. android_master - branch-ul pe care este codul Android
2. react-native_master - brach-ul pe care este codul React-native

BeeApp

Folosind aplicatia BeeApp utilizatorul poate deveni mai productiv, observand  timpul lucrat pe un task si respectand Goals-urile (Scopurile) setate.
Utilizatorul isi poate crea un cont. 
Dupa ce si-a creat cont utilizatorul isi poate seta primul Goal, de ex: "Programming 5 hours a day", or "Read 50 pages a day", etc.
Initial Goal-ul este prestabilit: "Work 5 hours a day", aceasta fiind editabil.
Utilizatorul va adauga taskuri terminate si timpul de lucru necesar pentru terminarea acelui task. Se pot adauga taskuri pentru ziua curenta, sau pentru zilele trecute.
Utilizatorul va vedea lista de taskuri adaugate. La selectarea unui task se va deschide o fereasca cu toate detaliile, unde taskul poate fi modificat sau sters.
Utilizatorul va avea posibilitatea sa vizualizeze un raport al taskurilor, reprezentat si sub forma de grafic, observand daca si-a atins sau nu scopul.

/* Daca utiliatorul isi atinge scopul, constant, i se da posibilitatea sa adauge si alte Scopuri (Goals), acestea toate fiind afisate intr-un Dashboard. */
/* Dashboard -> Goals -> Tasks -> Details */